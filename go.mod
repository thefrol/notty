module gitlab.com/thefrol/notty

go 1.21.1

require (
	github.com/caarlos0/env/v10 v10.0.0
	github.com/getkin/kin-openapi v0.120.0
	github.com/go-chi/chi v1.5.5
	github.com/go-chi/chi/v5 v5.0.10
	github.com/go-resty/resty/v2 v2.10.0
	github.com/gojuno/minimock/v3 v3.1.3
	github.com/google/uuid v1.3.1
	github.com/jackc/pgx/v5 v5.5.0
	github.com/mailru/easyjson v0.7.7
	github.com/oapi-codegen/runtime v1.0.0
	github.com/pressly/goose/v3 v3.15.1
	github.com/stretchr/testify v1.8.4
)

require (
	github.com/apapsch/go-jsonmerge/v2 v2.0.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-openapi/jsonpointer v0.19.6 // indirect
	github.com/go-openapi/swag v0.22.4 // indirect
	github.com/invopop/yaml v0.2.0 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20221227161230-091c0ba34f0a // indirect
	github.com/jackc/puddle/v2 v2.2.1 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826 // indirect
	github.com/perimeterx/marshmallow v1.1.5 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/rogpeppe/go-internal v1.11.0 // indirect
	golang.org/x/crypto v0.14.0 // indirect
	golang.org/x/net v0.17.0 // indirect
	golang.org/x/sync v0.3.0 // indirect
	golang.org/x/text v0.13.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

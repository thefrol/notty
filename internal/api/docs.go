// Содержит хендлеры и сервер HTTP API
// Из openapi спеки был сгенерирован код для сервера,
// причем так, что этот код предоставляет некоторый интерфейс
// который надо воплотить. Интерфейс состоит из ручек.
//
// В этой папке лежит как раз воплощение интерфейса, где
// все хендлеры привязаны к структурке. И ещё вторая структурка
// которая являет собой сервер.
//
// При появляении grpc api это все уедет в подпапку http
// и тут появлися папка grpc

package api
